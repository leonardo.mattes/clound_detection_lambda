

import os
from os import listdir
from os.path import isfile, join
import numpy as np

from PIL import Image

def validaimage(path ):
    imorigem = Image.open(path)
    image_array = np.array(imorigem)[0:192, 0:192, 0:3]
    if (image_array.shape[0] < 192) or  (image_array.shape[1] < 192) or ( image_array.shape[2] < 3) :
        return False
    return True

def validamask(path ):
    imagemask = Image.open(path)
    mask_array = np.array(imagemask)[0:192, 0:192]
    if (mask_array.shape[0] < 192) or  (mask_array.shape[1] < 192):
        return False
    return True

    
    
def validaitem(path_nuven,path_imagem ):
    try:
        if validaimage(path_imagem) and  validamask(path_nuven):
            return True
        else :
            return True
    except:
        return False

def existfonte(path, filename):
    filename.replace('_nuvem.tif', '.tif')
    filename = join(path, filename)
    if os.path.isfile(filename):
        return True
    else:
        return False
    
def getfiles(path, dirname):
    result = []
    pathdir = join(path, dirname)
    filesnuves = [join(pathdir, f) for f in listdir(pathdir) if (existfonte(pathdir, f)  and ('_nuvem.tif' in f)  )]
    return filesnuves


def getpaths(path):
    dirs =['cbres1','cbres2', 'sentinel1', 'sentinel2']
    filesname = []   
    for dirname in dirs:
        filesname =filesname + getfiles(path, dirname)
    

    filesnamesfinal = []        
    for path_nuven in filesname:
        path_imagem= path_nuven.replace('_nuvem.tif', '.tif') 
        if validaitem(path_nuven,path_imagem):
            filesnamesfinal.append((path_imagem,path_nuven ))
    len(filesnamesfinal)  , len(filesname)
    return filesnamesfinal

def getpathsnovo(path):
    dirs =['sentinel1_v2','sentinel2_v2']
    filesname = []   
    for dirname in dirs:
        filesname =filesname + getfiles(path, dirname)
    

    filesnamesfinal = []        
    for path_nuven in filesname:
        path_imagem= path_nuven.replace('_nuvem.tif', '.tif') 
        if validaitem(path_nuven,path_imagem):
            filesnamesfinal.append((path_imagem,path_nuven ))
    len(filesnamesfinal)  , len(filesname)
    return filesnamesfinal



def getImages(item):
     # image
    
    image = Image.open(item[0] )
    image_array = np.array(image)[0:192, 0:192,0:3]
    image_array= image_array.astype(np.float32)
    image_array /= 255
            # mask 
   
    imagemask = Image.open(item[1] )
    
    mask_array = np.array(imagemask)[0:192, 0:192]
    
    mask_array= mask_array.astype(np.float32)
    mask_array = mask_array[..., np.newaxis]
    return image_array, mask_array    

def array_imput_image(image_array):
    image_red = image_array[0,:,:]
    image_green = image_array[1,:,:]
    image_blue = image_array[2,:,:]
    image = np.stack((image_red, image_green, image_blue), 2)
    return image



def getImagessize(item, size):
     # image
    
    image = Image.open(item[0] )
    image_array = np.array(image)[0:size, 0:size,0:3]
    image_array= image_array.astype(np.float32)
    
    imagemask = Image.open(item[1] )
    
    mask_array = np.array(imagemask)[0:size, 0:size]
    
    mask_array= mask_array.astype(np.float32)
    mask_array = mask_array[..., np.newaxis]
    return image_array, mask_array    


# image array [x, y ,3]  -> imput imput size  (1, 3, x, y)
def imageArray_toImput(image_array):
    image_red = image_array[:,:,0]
    image_green = image_array[:,:,1]
    image_blue = image_array[:,:,2]
    imput = np.stack((image_red, image_green, image_blue), axis=0)
    imput = imput[np.newaxis, ...]
    imput= imput.astype(np.float32)
    return imput
