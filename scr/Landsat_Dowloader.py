import numpy as np
import boto3
import os
import sys
import json
from PIL import Image

#
class Landsat_Dowloader:

    def __init__(self, item):

        self.SAT_PREVIEW_BUCKET ='sat-preview.inceres.com.br'
        self.LANDSAT_STAC_BUCKET='usgs-landsat'
        self.SAT_PREVIEW_URL_BASE = 'https://sat-preview.inceres.com.br'
        self.product_id = item['product_id']

        #  b4 red  b3- green ; b2 Blue :   ,  b5 infra red #   b6 infra red distante
        self.list_extensions = ['_B4.TIF','_B3.TIF', '_B2.TIF']

        self.s3_bucket = 'landsat-pds'
        self.item = item

        self.paths = []

        path_imagens = os.getenv('path_imagens')
        self.save_path = os.path.join(path_imagens,  self.product_id)

        self.is_stac = self.SAT_PREVIEW_BUCKET in self.item['url']
        self.s3_bucket = self.LANDSAT_STAC_BUCKET if self.is_stac else self.s3_bucket

    def __format_stac_url(self):
        scene_path = self.item['url'].replace('{}/'.format(self.SAT_PREVIEW_URL_BASE), '')
        last_part = scene_path.split('/')[-1]
        return scene_path.replace(last_part, '')

    def __format_url_new(self):
        return 'c1/{}{}/{}/{}/{}/'.format(
            self.product_id[0],
            self.product_id[3],
            self.product_id[10:13],
            self.product_id[13:16],
            self.product_id
        )

    def __format_url_old(self):
        return '{}{}/{}/{}/{}/'.format(
            self.product_id[0],
            self.product_id[2],
            self.product_id[3:6],
            self.product_id[6:9],
            self.product_id
        )

    def format_landsat_ulr(self):
        if len(self.product_id) > 21:
            return self.format_landsat_ulr_new()
        return self.format_landsat_ulr_old()

    def __format_url(self):
        if self.is_stac:
            return self.__format_stac_url()
        if len(self.product_id) > 21:
            return self.__format_url_new()
        return self.__format_url_old()

    def download_scene(self):
        list_path_image = []
        try:
            path_url = self.__format_url()

            for extension in self.list_extensions:
                filename_download = '{}{}'.format(self.product_id, extension)
                path_url_download = '{}{}'.format(path_url, filename_download)
                self.paths.append(path_url_download)
                #path_save = '{}/{}'.format(self.path_slug, filename_download)
                #list_path_image.append(path_save)
                #self._download_using_boto(path_url_download, path_save, self.s3_bucket)

            #self.stater.set_scene_to_finished(self.entity_id)
        except Exception as error:
            print(error)




    def setLimits(self):
        self.item['coordinates'][0]
        tatis = []
        longs = []
        for cor in self.item['coordinates'][0]:
            tatis.append(cor[1])
            longs.append(cor[0])

        self.lonmin = min(longs)
        self.lonmax = max(longs)
        self.latmin = min(tatis)
        self.latmax = max(tatis)
        self.deltalong = self.lonmax - self.lonmin
        self.deltalat = self.latmax - self.latmin

    def dowloadPaths(self):
        if not os.path.isdir(self.save_path):
            print("criando diretorio em ", self.save_path)
            os.mkdir(self.save_path)

        s3 = boto3.resource('s3')
        bucket = s3.Bucket(self.s3_bucket)
        for path in self.paths:
            pathlocal = self.save_path + "/" + path[-7:]
            print("baixando arquivo : ", self.s3_bucket,  path, pathlocal)
            #bucket.download_file(path, pathlocal)
            bucket.download_file(path, pathlocal, ExtraArgs={'RequestPayer': 'requester'})

        with open(self.save_path + '/data.json', 'w') as fp:
            json.dump(self.item, fp)

    # get retrato a
    def getRetrato(self, pathimagem, longitudemin, longitudemax, latitudemin, latitudemax) :
        im = Image.open(pathimagem)
        imarray = np.array(im)
        nlong = imarray.shape[0]
        nlat = imarray.shape[1]
        steplong = self.deltalong / nlong
        steplat = self.deltalat / nlat
        long_ini = int((longitudemin - self.lonmin) / steplong) - 2
        long_fim = int((longitudemax - self.lonmin) / steplong) + 2
        lat_ini = int((latitudemin - self.latmin) / steplong) - 2
        lat_fim = int((latitudemax - self.latmin) / steplong) + 2
        self.resultm = np.zeros((long_fim - long_ini, lat_fim - lat_ini, 3))

        for i_log in range(long_ini, long_fim): # longitude
            for j_lat in range(lat_ini, lat_fim): # latitude
                i = i_log - long_ini
                j = j_lat - lat_ini
                self.resultm[i, j, 0] = imarray[i_log, j_lat]  # valor
                self.resultm[i, j, 1] = self.lonmin + (i_log * steplong)  # longitude
                self.resultm[i, j, 2] = self.latmin + (j_lat * steplat)  # longitude

        return self.resultm
