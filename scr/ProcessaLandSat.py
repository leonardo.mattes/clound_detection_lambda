import Landsat_Dowloader as landDow
import image_util as util
import Predic as modelLoader
import numpy as np
import os
from PIL import Image
import torch

listimad = ['_B4.TIF', '_B3.TIF', '_B2.TIF']  # rgb
fator = 65535 / 255  # landsat


def norm1(band):
    return np.divide(band, fator)

def dowLoadImage(url, product_id):
    item = {
        "url": url,
        "product_id": product_id,
    }
    landDowloder = landDow.Landsat_Dowloader(item)
    landDowloder.download_scene()  # cria os links
    landDowloder.dowloadPaths()


def getImage(url, product_id, polygon_wkt):
    path_imagens = os.getenv('path_imagens')
    pathbase = os.path.join(path_imagens, product_id)
    arrays = []
    for imadph in listimad:
        pathimage = os.path.join(pathbase, imadph)
        array = util.getimageWindow(pathimage, polygon_wkt)
        arrays.append(array)

    array4 = norm1(arrays[0])
    array3 = norm1(arrays[1])
    array2 = norm1(arrays[2])
    rgb = np.dstack((array4, array3, array2))
    rgb = rgb.astype(np.uint8)
    p = Image.fromarray(rgb)
    # path = join(outputdir, pathim)
    pathoput = os.path.join(pathbase, 'rgb.tif')
    print('salvando', pathoput)
    p.save(pathoput)


def predict(product_id):
    path_imagens = os.getenv('path_imagens')
    path_image = os.path.join(path_imagens, product_id, 'rgb.tif')

    imagearray = util.getImagefull(path_image)
    imput = util.imageArray_toImputSolo(imagearray)

    model = modelLoader.loadModel() #imput (1, 3, 400, 400)
    with torch.no_grad():
        outputs = model(torch.tensor(imput))

    return outputs.numpy(), imagearray


def processa(url, product_id, polygon_wkt):
    dowLoadImage(url, product_id)
    getImage(url, product_id, polygon_wkt)
    return predict(product_id)




