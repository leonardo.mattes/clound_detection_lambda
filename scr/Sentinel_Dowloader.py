import boto3
import os
import sys
import json
from PIL import Image

URL_SENTINEL2='http://sentinel-s2-l1c.s3-website.eu-central-1.amazonaws.com'

#
class Sentinel_Dowloader:

    def __init__(self, item, completo =1):
        self.scene_id = item['scene_id']
         # b3- green ;  b4 red ,  b5 infra red #   b6 infra red distante
        # b3- green ;  b4 red ,  b5 infra red #   b6 infra red distante
        if completo == 1:
            self.list_extensions = ['B03.JP2','B04.JP2','B05.JP2', 'B06.JP2', 'B07.JP2' 'B08.JP2', 'B08a.JP2', 'B11.JP2']
        else:
            self.list_extensions = ['B03.JP2']

        self.list_extensions = ['B03.JP2']

        self.s3_bucket = 'sentinel-s2-l1c'
        self.s3_bucket = 'sentinel-cogs'

        self.item = item
        self.paths = []
        path_imagens = os.getenv('path_imagens')
        self.save_path = os.path.join(path_imagens, self.scene_id)
        if  'roda.sentinel-hub.com' in  item['url']:
            self.is_from_stac = True
            self.s3_bucket = 'sentinel-cogs'
        else:
            self.is_from_stac = False
            self.s3_bucket = 'sentinel-s2-l1c'



    def format_url_v(self, scene_id):

        url = 's3://sentinel-s2-l1c/' + "/tiles/{}/{}/{}/{}/{}/{}"
        if scene_id.startswith('S2A_'):
            month = scene_id[29:31]
            day = scene_id[31:33]
            id_image = scene_id[50:52]
            word_1_image = scene_id[52]
            word_2_image = scene_id[53:55]
            year_image = scene_id[25:29]
        else:
            month = scene_id[23:25]
            day = scene_id[25:27]
            id_image = scene_id[5:7]
            word_1_image = scene_id[7]
            word_2_image = scene_id[8:10]
            year_image = scene_id[19:23]

        if month[0] == '0':
            month = month[1]
        if day[0] == '0':
            day = day[1]
        if id_image[0] == '0':
            id_image = id_image[1]

        self.url_path = url.format(id_image, word_1_image, word_2_image, year_image, month, day)
        return self.url_path

    def format_url(self, scene_id):
        if self.is_from_stac:
            return 'sentinel-s2-l2a-cogs/{}/{}/{}/{}/{}/{}'.format(
                self.scene_id[4:6],
               self.scene_id[6:7],
                self.scene_id[7:9],
                self.scene_id[10:14],
                int(self.scene_id[14:16]),
                self.scene_id
            )

        url = URL_SENTINEL2 + "/tiles/{}/{}/{}/{}/{}/{}/0"
        if scene_id.startswith('S2A_'):
            month = scene_id[29:31]
            day = scene_id[31:33]
            id_image = scene_id[50:52]
            word_1_image = scene_id[52]
            word_2_image = scene_id[53:55]
            year_image = scene_id[25:29]
        else:
            month = scene_id[23:25]
            day = scene_id[25:27]
            id_image = scene_id[5:7]
            word_1_image = scene_id[7]
            word_2_image = scene_id[8:10]
            year_image = scene_id[19:23]

        if month[0] == '0':
            month = month[1]
        if day[0] == '0':
            day = day[1]
        if id_image[0] == '0':
            id_image = id_image[1]

        url_path = url.format(id_image, word_1_image, word_2_image, year_image, month, day)
        return url_path


    def geturls(self, list_band_download):
        self.paths = self.format_pizza_band_names(list_band_download)

        return self.paths

    def format_pizza_band_names(self, list_band_download):
        url_path = self.format_url(self.scene_id)
        bands = []
        extension = 'tif' if self.is_from_stac else 'jp2'
        #extension = 'jp2'

        for band in list_band_download:
            band_link = '{}/{}'.format(url_path, band)

            bands.append(band_link)
        return bands




    def setLimits(self):
        self.item['coordinates'][0]
        tatis = []
        longs = []
        for cor in self.item['coordinates'][0]:
            tatis.append(cor[1])
            longs.append(cor[0])

        self.lonmin = min(longs)
        self.lonmax = max(longs)
        self.latmin = min(tatis)
        self.latmax = max(tatis)
        self.deltalong = self.lonmax - self.lonmin
        self.deltalat = self.latmax - self.latmin

    def dowloadPaths(self):
        if not os.path.isdir(self.save_path):
            print("criando diretorio em ", self.save_path)
            os.mkdir(self.save_path)

        s3 = boto3.resource('s3')
        bucket = s3.Bucket(self.s3_bucket)

        for path in self.paths:
            pathlocal = self.save_path + "/" + path[-7:]

            if not self.is_from_stac:
                path = path[61:]

            print("baixando arquivo em " + path)
            bucket.download_file(path, pathlocal, ExtraArgs={'RequestPayer': 'requester'})
            #bucket.download_file(path, pathlocal)

        with open(self.save_path + '/data.json', 'w') as fp:
            json.dump(self.item, fp)





