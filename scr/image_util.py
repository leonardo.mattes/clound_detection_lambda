
from random import randrange
from PIL import Image
import os
from os import listdir
from os.path import isfile, join
import numpy as np
from PIL import Image
import rasterio
import rasterio.features
import rasterio.warp


from datetime import datetime, timedelta
from rasterio.io import MemoryFile
from rasterio.windows import Window
from rasterio.features import geometry_window
from rasterio.features import rasterize
import pyproj
from shapely import wkt
from shapely.ops import transform
from shapely.geometry import mapping
import numpy as np


# The window needs to be rounded to a power of 2 large enougth not to cause problems with the model:
def round_window(w, r=256):
    wnp = np.array(w.toranges())
    sw = np.diff(wnp).flatten()
    nw = np.int0(r * np.ceil(sw / r))
    dw = np.int0((nw - sw) // 2)
    if wnp[:, 0][0] > 0 and wnp[:, 0][1] > 0:
        rc = wnp[:, 0] - dw
    else:
        rc = wnp[:, 0]
        nw = nw + dw
    return Window(row_off=rc[0], col_off=rc[1], width=nw[0], height=nw[1])

def round_window2(w, r=256):
    cordinates = w.toranges()
    positions =[ cordinates[1][0],cordinates[0][0] ] # col , row
    positions_final = [cordinates[1][1], cordinates[0][1]] # col , row
    witdh = positions_final[0] - positions[0]
    height = positions_final[1] - positions[1]
    if (positions_final[0]- positions[0]) %  r >0:
        witdh = witdh - ((positions_final[0]- positions[0]) %  r) + r
    if (positions_final[1]- positions[1]) %  r >0:
        height =  height + ((positions_final[1]- positions[1]) %  r) +  r


    return Window(row_off=positions[1], col_off=positions[0], width=witdh, height=height)

def getimageWindow(path, polygon_wkt):
    pol = wkt.loads(polygon_wkt)

    src = rasterio.open(path, mode='r')
    dst_profile = src.profile.copy()
    src_crs = src.crs

    # Create the src and dst projections:
    utm_proj = pyproj.CRS(src_crs)
    wgs_proj = pyproj.CRS("EPSG:4326")

    # Create the functions to convert to and from utm:
    to_utm = pyproj.Transformer.from_crs(wgs_proj, utm_proj, always_xy=True)

    # Transform the polygon to utm:
    pol = transform(to_utm.transform, pol)

    # Get the row, col indices of the polygon bounds inside the raster:
    pol_w = geometry_window(src, [pol])

    # Get the tranformation for the output raster:
    msk_w = src.window_transform(pol_w)
    # Round the indices to get a consistent Window:
    w = round_window(pol_w)

    # Get the tranformation for the temp raster:
    # sw = src.window_transform(w)

    array = src.read(1, window=w)
    return array

def norm(band):
    
    band_min, band_max = band.min(), band.max()
    #print('banda',band_min, band_max, band.mean() )
    #fator = 1 / band.max()
    fator = 2
    nband = band * fator
    #print('-- banda 2',nband.min(), nband.max(), nband.mean() )
    return nband

def formatPoligono(longitudeLimites, latitudeLimites):
    polygon_wkt = 'POLYGON (({:.13f} {:.13f},{:.13f} {:.13f},{:.13f} {:.13f},{:.13f} {:.13f}, {:.13f} {:.13f}))'.format(longitudeLimites[0], latitudeLimites[0],
                                                                      longitudeLimites[1], latitudeLimites[0],
                                                                      longitudeLimites[1], latitudeLimites[1],
                                                                      longitudeLimites[0], latitudeLimites[1],
                                                                      longitudeLimites[0], latitudeLimites[0])
    return polygon_wkt

def formatPoligonoLimites(limites):
    polygon_wkt = 'POLYGON (({:.13f} {:.13f},{:.13f} {:.13f},{:.13f} {:.13f},{:.13f} {:.13f}, {:.13f} {:.13f}))'.format(limites[0][0], limites[0][1],
                                                                      limites[1][0], limites[1][1],
                                                                      limites[2][0], limites[2][1],
                                                                      limites[3][0], limites[3][1],
                                                                      limites[4][0], limites[4][1])
    return polygon_wkt




def imageArray_toImput(image_array):
    image_red = image_array[:,:,0]
    image_green = image_array[:,:,1]
    image_blue = image_array[:,:,2]
    imput = np.stack((image_red, image_green, image_blue), axis=0)
    
    imput= imput.astype(np.float32)
    return imput

# transforma em bach de dimensao unica
def imageArray_toImputSolo(image_array):
    image_array = imageArray_toImput(image_array)
    image_array = image_array[np.newaxis, ...]
    return image_array


def getImagefull(path):
    image = Image.open(path)
    image_array = np.array(image)[:, :, 0:3]
    image_array = image_array.astype(np.float32)
    image_array /= 255

    return norm(image_array)

def getImage(path, size):
    image = Image.open(path )
    image_array = np.array(image)[0:size, 0:size,0:3]
    image_array= image_array.astype(np.float32)
    image_array /= 255
    
    return norm(image_array)

def getmasks(path, size):
    imagemask = Image.open(path)
    mask_array = np.array(imagemask)[0:size, 0:size]
    
    
    mask_array= mask_array.astype(np.float32)
    
    if mask_array.max() > 1 :
        mask_array[mask_array  <= 50] = 0
        mask_array[mask_array > 50] = 1
        
        
        
    #print('mask_array',mask_array.min(), mask_array.max()  )    
    return  mask_array[:,:,0]


def getImages(item, size):
    return getImage(item[0], size),  getmasks(item[1], size)


