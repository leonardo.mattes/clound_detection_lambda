
import os
import ProcessaSentinel as sentinel
import ProcessaLandSat as landsat
import urllib
import pytorch_unet as pytorch_unet
import torch
import numpy as np
import json
import urllib.request

def loadModel():

    pathmodel = os.getenv('path_model_bin')
    pathtemp = os.environ["path_imagens"]
    pathModelLocal = pathtemp + "model.pt"


    urllib.request.urlretrieve(pathmodel, pathModelLocal)


    modelo = pytorch_unet.UNet(1)
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    model = modelo.to(device)
    model.load_state_dict(torch.load(pathModelLocal, map_location='cpu'))
    model.eval()
    return model


def predicClound(product_id, url , satelite, polygon_wkt ):

    if satelite == 'sentinel':
        resposta= sentinel.processa(url, product_id, polygon_wkt)
    elif satelite == 'landsat':
        resposta= landsat.processa(url, product_id, polygon_wkt)

    resposta[0][resposta[0] < 0.5] = 0
    resposta[0][resposta[0] >= 0.5] = 1
    clound_percent = (1 - np.mean(resposta[0])) * 100
    response = json.dumps({
        "cloud": clound_percent,
        "rgb": resposta[1].tolist(),
        "msk": resposta[0].tolist()
    })
    return response

