import scr.CloudLambda as lambdafunction
import scr.Predic as predic
import os

os.environ["path_model_bin"] = 'https://testesleo.s3.us-east-2.amazonaws.com/model_new_355.pt'
os.environ["path_imagens"] ='/tmp/'
#predic.loadModel()

event= {'product_id': 'L1C_T22KDF_A029436_20210209T134209',
 'url': 'http://sentinel-s2-l1c.s3-website.eu-central-1.amazonaws.com/tiles/22/K/DF/2021/2/24/0/preview.jpg',
 'satelite': 'sentinel',
 'polygon_wkt': 'POLYGON ((-51.1066360000000 -18.0045970000000,-51.0771300000000 -18.0045970000000,-51.0771300000000 -17.9448500000000,-51.1066360000000 -17.9448500000000, -51.1066360000000 -18.0045970000000))'}
# api-endpoint

result = lambdafunction.lambda_handler(event, '')
print(result)