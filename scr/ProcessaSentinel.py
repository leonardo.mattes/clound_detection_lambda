import Sentinel_Dowloader as sentinelDow
import image_util as util
import Predic as modelLoader
import numpy as np
import os
from PIL import Image
import torch

listimad = 'B04.jp2', 'B03.jp2', 'B02.jp2'
#fator = 65535 / 255  # landsat
fator = 12000 /500

def norm1(band):
    return np.divide(band, fator)

def dowLoadImage(url, product_id):
    item = {
        "url": url,
        "product_id": product_id,
        "scene_id": product_id,

    }
    sentDowloder = sentinelDow.Sentinel_Dowloader(item)
    list_band_download = ['B04.jp2', 'B03.jp2', 'B02.jp2', 'preview.jpg']
    sentDowloder.geturls(list_band_download)
    sentDowloder.dowloadPaths()



def getImage(url, product_id, polygon_wkt):
    path_imagens = os.getenv('path_imagens')
    pathbase = os.path.join(path_imagens, product_id)

    arrays = []
    for imadph in listimad:
        pathimage = os.path.join(pathbase, imadph)
        array = util.getimageWindow(pathimage, polygon_wkt)
        arrays.append(array)

    array4 = norm1(arrays[0])
    array3 = norm1(arrays[1])
    array2 = norm1(arrays[2])
    rgb = np.dstack((array4, array3, array2))
    rgb = rgb.astype(np.uint8)
    p = Image.fromarray(rgb)
    pathoput = os.path.join(pathbase, 'rgb.tif')
    print('salvando', pathoput)
    p.save(pathoput)

    #plt.imshow(rgb, interpolation='nearest')
    #print(rgb.shape)


def predict(product_id):
    path_imagens = os.getenv('path_imagens')
    path_image = os.path.join(path_imagens, product_id, 'rgb.tif')
    imagArray = util.getImagefull(path_image)
    imput = util.imageArray_toImputSolo(imagArray)

    model = modelLoader.loadModel()
    with torch.no_grad():
        outputs = model(torch.tensor(imput))

    return outputs.numpy(), imagArray


def processa(url, product_id, polygon_wkt):
    dowLoadImage(url, product_id)
    getImage(url, product_id, polygon_wkt)
    return predict(product_id)


if __name__ == "__main__":
    os.environ["path_model_bin"] = '/home/desenvolvimento/inceres/image_seg/bin/model_new_355.pt'
    os.environ["path_imagens"] = '/home/desenvolvimento/inceres/imagens/'
    product_id = 'L1C_T22KDF_A029436_20210209T134209'
    #polygon_wkt = 'POLYGON ((-51.1066360000000 18.0045970000000,51.0771300000000 18.0045970000000,51.0771300000000 -17.9848500000000,-51.1066360000000 -17.9848500000000, -51.1066360000000 18.0045970000000))'
    polygon_wkt = 'POLYGON ((-51.1066360000000 -18.0045970000000,-51.0971300000000 -18.0045970000000,-51.0771300000000 -17.9448500000000,-51.1066360000000 -17.9448500000000, -51.1066360000000 -18.0045970000000))'
    polygon_wkt ='POLYGON ((-51.1066360000000 -18.0045970000000,-51.0971300000000 -18.0045970000000,-51.0771300000000 -17.9448500000000,-51.1066360000000 -17.9448500000000, -51.1066360000000 -18.0045970000000))'
    polygon_wkt ='POLYGON ((-51.1066360000000 18.0045970000000,51.0971300000000 18.0045970000000,51.0971300000000 -17.9448500000000,-51.1066360000000 -17.9448500000000, -51.1066360000000 18.0045970000000))'
    url = 'http://sentinel-s2-l1c.s3-website.eu-central-1.amazonaws.com/tiles/22/K/DF/2021/2/24/0/preview.jpg'
    getImage(url, product_id, polygon_wkt)
    predict(product_id)
