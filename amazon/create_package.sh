mkdir functionlib


python3.8 -m pip install --target functionlib Pillow==8.4.0
python3.8 -m pip install --target functionlib pyProj==3.0.1
python3.8 -m pip install --target functionlib Shapely
rm -rf functionlib/numpy


cd functionlib
zip -r ../package.zip .

cd ../../scr
zip -g ../amazon/package.zip *.py

